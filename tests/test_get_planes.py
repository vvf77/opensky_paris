from opensky_paris import get_planes, paris_lng, paris_lat, distance
import unittest

try:
    from unittest import mock
except ImportError:
    import mock

test_item_inside = ['', 'inside', 0, 0, 0, 2.3488, 48.85341]  # Paris
test_item_outside_far = ['', 'outside', 0, 0, 0, 44.496632, 48.717732]  # Volgograd
test_item_outside_near = ['', 'outside', 0, 0, 0, 7.219709, 51.473142]  # Bochum (455 km)

fake_response = mock.MagicMock()
fake_response.json = mock.MagicMock(return_value={'states': [
    test_item_inside,
    test_item_outside_near,
    test_item_outside_far
]})


class GetPlanesTest(unittest.TestCase):
    def testAllResultOnRealApi(self):
        results = get_planes()
        print('near paris count= %s' % len(results))
        assert len(results) > 0
        for plane in results:
            latitude, longitude = plane['latitude'], plane['longitude']
            dist = distance(latitude, longitude, paris_lat, paris_lng)
            assert dist < 450000

    @mock.patch('opensky_paris.requests')
    def testFilteringResuls(self, mocked_requests):
        mocked_requests.get = mock.MagicMock(return_value=fake_response)
        result = get_planes()
        self.assertEqual(len(result), 1)
        self.assertEqual(result[0]['callsign'], 'inside')


if __name__ == '__main__':
    unittest.main()
