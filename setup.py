from setuptools import setup, find_packages
from os.path import join, dirname
import unittest

__version__ = "0.0.1"


def test_suite():
    test_loader = unittest.TestLoader()
    return test_loader.discover('tests', pattern='test_*.py')


setup(
    name='opensky_paris',
    version=__version__,
    description='Fetch all planes near Paris',
    author='Vladimir Fedorenko',
    author_email='vvf7723@gmail.com',
    packages=find_packages(),
    include_package_data=True,
    long_description=open(join(dirname(__file__), 'readme.md')).read(),
    url='https://bitbucket.org/vvf77/opensky_paris',
    scripts=[],
    zip_safe=False,
    install_requires=[
        'requests==2.18.4',
    ],
    test_suite='setup.test_suite',
    tests_require=[
        'mock==2.0.0',
    ]
)
