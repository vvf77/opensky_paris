Simple library has only one function "get_planes" which return all planes
near Paris (inside about 450 kilometres circle of Paris).
Function "get_planes" returns list of dict with keys:
'callsign', 'longitude' and 'latitude'.

Installation
------------

to install run `pip install git+https://bitbucket.org/vvf77/opensky_paris`

Example usage
-------------

```
#!python
#
from opensky_paris import get_planes
for plane in get_planes():
    print(plane['callsign'])
```