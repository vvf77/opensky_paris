import requests
import math

api_url = 'https://opensky-network.org/api/states/all'

paris_lat = 48.85341
paris_lng = 2.3488
max_dlat = 4.0458073
max_dlng = 6.1504326
max_dlat_square = max_dlat * max_dlat
max_dlng_square = max_dlng * max_dlng

EARTH_RADIUS = 6371210  # it depends on measure and variates from 6371210 to 6378168


def distance(deg_lat1, deg_lng1, deg_lat2, deg_lng2):
    if abs(deg_lat1 - deg_lat2) <= 0.00001 and abs(deg_lng1 - deg_lng2) <= 0.00001:
        return 0

    # radianes
    lat1 = deg_lat1 * math.pi / 180.
    lat2 = deg_lat2 * math.pi / 180.
    lngg1 = deg_lng1 * math.pi / 180.
    lngg2 = deg_lng2 * math.pi / 180.

    # cos ans sin of differences
    cl1 = math.cos(lat1)
    cl2 = math.cos(lat2)
    sl1 = math.sin(lat1)
    sl2 = math.sin(lat2)
    delta = lngg2 - lngg1
    cdelta = math.cos(delta)
    sdelta = math.sin(delta)

    # calculate length on big circle
    y = math.sqrt(math.pow(cl2 * sdelta, 2) + math.pow(cl1 * sl2 - sl1 * cl2 * cdelta, 2))
    x = sl1 * sl2 + cl1 * cl2 * cdelta
    ad = math.atan2(y, x)
    dist = ad * EARTH_RADIUS
    return dist


def is_near_paris(latitude, longitude):
    """
    Checks if geo-point (arguments latitude and longitude) is inside ellipse
    :return: True - is inside 450 km zone of Paris, False - far from Paris more than 450 km.
    """
    if not latitude or not longitude:
        return False
    delta_lat = paris_lat - latitude
    delta_lng = paris_lng - longitude
    if delta_lat * delta_lat / max_dlat_square + delta_lng * delta_lng / max_dlng_square > 1:
        return False
    return distance(latitude, longitude, paris_lat, paris_lng) <= 450000


def get_planes():
    api_response = requests.get(api_url)
    data = api_response.json()
    planes = ({'callsign': plane[1], 'longitude': plane[5], 'latitude': plane[6]}
              for plane in data['states']
              if plane[5] and plane[6])
    return [plane for plane in planes if is_near_paris(plane['latitude'], plane['longitude'])]
